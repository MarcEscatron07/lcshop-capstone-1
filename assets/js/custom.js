$(document).ready(function(){

// Start of Jerseys
	// 1st row of Jerseys
	$(".prd-jersey-0").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-clg-back.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-0").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-clg.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-jersey-1").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-tl-back.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-1").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-tl.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-jersey-2").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-c9-back.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-2").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-c9.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-jersey-3").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-fox-back.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-3").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-fox.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	// 2nd row of Jerseys
	$(".prd-jersey-4").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-tsm-back.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-4").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-tsm.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-jersey-5").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-fly-back.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-5").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-fly.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-jersey-6").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-opt-back.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-6").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-opt.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-jersey-7").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-clutch-back.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-7").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-clutch.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	// last row of Jerseys
	$(".prd-jersey-8").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-ggs-back.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-8").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-ggs.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-jersey-9").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-100t-back.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-9").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-100t.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-jersey-10").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-clg-alt-back.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-jersey-10").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/jerseys/jersey-clg-alt.png").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
// End of Jerseys

// Start of Casual Wear
	// 1st row of Casual Wear
	$(".prd-casual-0").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-back-blue.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-casual-0").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-tl.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-casual-1").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-back-black.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-casual-1").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-clg.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-casual-2").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-back-black.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-casual-2").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-clutch.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-casual-3").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-back-lightblue.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-casual-3").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-ggs.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	// last row of Casual Wear
	$(".prd-casual-4").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-back-black.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-casual-4").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-opt.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});

	$(".prd-casual-5").mouseenter(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-back-black.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
	$(".prd-casual-5").mouseleave(async function(){
		$(this).css({'cursor' : 'wait'});
		$(this).fadeTo(400, 1, async function(){
			await $(this).attr("src","assets/images/shop/casual/casual-tee-fly.webp").one("load", function () {
				$(this).css({'cursor' : 'default'});
			});
		});		
	});
// End of Casual Wear
});